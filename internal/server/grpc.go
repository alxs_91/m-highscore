package grpc

import (
	"context"
	"fmt"
	"net"

	pbFileScore "bitbucket.org/alxs_91/m-apis/m-highscore/v1"
	grpcGoogle "google.golang.org/grpc"
)

type Grpc struct {
	address string
	svr     *grpcGoogle.Server
}

var (
	HighScore = 999999999.0
)

func NewServer(address string) *Grpc {
	return &Grpc{
		address: address,
	}
}

func (g *Grpc) SetHighScore(ctx context.Context, input *pbFileScore.SetHighScoreRequest) (*pbFileScore.SetHighScoreResponse, error) {
	fmt.Print("Set High score")
	HighScore = input.HighScore
	return &pbFileScore.SetHighScoreResponse{Set: true}, nil
}

func (g *Grpc) GetHighScore(ctx context.Context, input *pbFileScore.GetHighScoreRequest) (*pbFileScore.GetHighScoreResponse, error) {
	fmt.Print("Get High score")
	return &pbFileScore.GetHighScoreResponse{HighScore: HighScore}, nil
}

func (g *Grpc) ListenAndServe() error {
	lis, err := net.Listen("tcp", g.address)
	if err != nil {
		return err
	}

	g.svr = grpcGoogle.NewServer()
	pbFileScore.RegisterGameServer(g.svr, g)

	fmt.Println("Running on: " + g.address)

	err = g.svr.Serve(lis)
	if err != nil {
		return err
	}

	return nil
}
