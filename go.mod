module bitbucket.org/alxs_91/m-highscore

go 1.14

require (
	bitbucket.org/alxs_91/m-apis v0.0.0-20200718200953-2ba5c7ab1ea1
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	google.golang.org/grpc v1.30.0
	google.golang.org/protobuf v1.25.0 // indirect
)
