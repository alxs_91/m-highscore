package main

import (
	"flag"
	"fmt"

	server "bitbucket.org/alxs_91/m-highscore/internal/server"
)

func main() {
	var addressPtr = flag.String("address", ":50051", "address connection")
	flag.Parse()

	s := server.NewServer(*addressPtr)
	err := s.ListenAndServe()

	if err != nil {
		fmt.Println(err)
	}

}
